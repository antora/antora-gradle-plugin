'use strict'

module.exports.register = function () {
  this.once('playbookBuilt', () => {
    console.log(`IS_TTY=${process.env.IS_TTY || '<not set>'}`)
    console.log(`FORCE_COLOR=${process.env.FORCE_COLOR || '<not set>'}`)
    this.stop()
  })
}
