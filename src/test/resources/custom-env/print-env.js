'use strict'

module.exports.register = function () {
  this.once('playbookBuilt', ({ playbook }) => {
    console.log(`HOME: ${playbook.env.HOME}`)
  })
}
