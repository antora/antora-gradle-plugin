'use strict'

module.exports.register = function () {
  this.once('playbookBuilt', ({ playbook }) => {
    console.dir(playbook.asciidoc.attributes)
    this.stop()
  })
}
