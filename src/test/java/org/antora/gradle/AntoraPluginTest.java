package org.antora.gradle;

import com.github.gradle.node.NodeExtension;
import com.github.gradle.node.NodePlugin;
import com.github.gradle.node.npm.task.NpxTask;
import com.github.gradle.node.util.PlatformHelper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.BranchConfig;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.RepositoryBuilder;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.logging.configuration.ShowStacktrace;
import org.gradle.api.tasks.TaskProvider;
import org.gradle.internal.impldep.org.testng.annotations.Parameters;
import org.gradle.testfixtures.ProjectBuilder;
import org.gradle.testkit.runner.BuildResult;
import org.gradle.testkit.runner.GradleRunner;
import org.gradle.testkit.runner.TaskOutcome;
import org.gradle.util.GradleVersion;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.CopyOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.contentOf;

@Parameters
class AntoraPluginTest {
    @TempDir
    File projectDirParent;

    @ValueSource
    public static List<GradleVersion> gradleVersions() {
        return List.of("7.3.3", "7.6").stream().map(GradleVersion::version).collect(Collectors.toList());
    }

    @Test
    void antoraPluginAddsAntoraExtensionBlock() throws Exception {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(AntoraPlugin.class);
        Object antora = project.getExtensions().findByName("antora");
        assertThat(antora).isNotNull();
        assertThat(antora).isInstanceOf(AntoraExtension.class);
    }

    @Test
    void antoraPluginRegistersAntoraTask() throws Exception {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(AntoraPlugin.class);
        TaskProvider<Task> antoraTaskProvider = project.getTasks().named("antora");
        assertThat(antoraTaskProvider.isPresent()).isTrue();
        Task antoraTask = antoraTaskProvider.get();
        assertThat(antoraTask).isInstanceOf(NpxTask.class);
        assertThat(antoraTask.getDescription()).contains("Runs Antora");
    }

    @Test
    void antoraPluginAppliesNodePlugin() throws Exception {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(AntoraPlugin.class);
        assertThat(project.getPlugins().findPlugin(NodePlugin.class)).isNotNull();
        Object node = project.getExtensions().findByName("node");
        assertThat(node).isNotNull();
        assertThat(node).isInstanceOf(NodeExtension.class);
    }

    @Test
    void antoraPluginEnablesDownloadOnNodeExtensionObjectByDefault() throws Exception {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(AntoraPlugin.class);
        NodeExtension node = project.getExtensions().getByType(NodeExtension.class);
        assertThat(node.getDownload().get()).isTrue();
    }

    @Test
    void antoraWithDefaults() throws Exception {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(AntoraPlugin.class);
        NpxTask antoraTask = (NpxTask) project.getTasks().getByPath(":antora");
        assertThat(antoraTask.getCommand().get()).isEqualTo("--yes");
        assertThat(antoraTask.getArgs().get()).containsOnly("--package", "antora", "antora", "antora-playbook.yml");
        Map<String, String> env = antoraTask.getEnvironment().get();
        assertThat(env).extracting("npm_config_cache").isEqualTo(project.file(".gradle/npm").toString());
        assertThat(env).extracting("npm_config_lockfile_version").isEqualTo("3");
        assertThat(env).extracting("FORCE_COLOR").isEqualTo("true");
        assertThat(env).extracting("IS_TTY").isEqualTo("true");
        assertThat(env).extracting("NODE_OPTIONS").isEqualTo("--no-global-search-paths");
        AntoraExtension antora = project.getExtensions().getByType(AntoraExtension.class);
        assertThat(antora.getPlaybookProvider()).isNotNull();
        assertThat(antora.getPlaybookProvider().isPresent()).isFalse();
        assertThat(antora.getPlaybookProvider().getRepository().isPresent()).isFalse();
        assertThat(antora.getPlaybookProvider().getBranch().get()).isEqualTo("docs-build");
    }

    @Test
    void antoraWithDefaultsShowStacktrace() throws Exception {
        Project project = ProjectBuilder.builder().build();
        project.getGradle().getStartParameter().setShowStacktrace(ShowStacktrace.ALWAYS);
        project.getPlugins().apply(AntoraPlugin.class);
        NpxTask antoraTask = (NpxTask) project.getTasks().getByPath(":antora");
        assertThat(antoraTask.getCommand().get()).isEqualTo("--yes");
        assertThat(antoraTask.getArgs().get()).containsOnly("--package", "antora", "antora", "--stacktrace",
            "antora-playbook.yml");
    }

    @Test
    void antoraWhenCustomPlaybookAsString() throws Exception {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(AntoraPlugin.class);
        AntoraExtension antora = project.getExtensions().getByType(AntoraExtension.class);
        antora.setPlaybook("my-antora-playbook.yml");
        NpxTask antoraTask = (NpxTask) project.getTasks().getByPath(":antora");
        assertThat(antoraTask.getArgs().get()).containsOnly("--package", "antora", "antora", "my-antora-playbook.yml");
    }

    @Test
    void antoraWithPlaybookProviderOnGitHub() throws Exception {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(AntoraPlugin.class);
        AntoraExtension antora = project.getExtensions().getByType(AntoraExtension.class);
        antora.getPlaybookProvider().getRepository().set("foo/bar");
        assertThat(antora.getPlaybookProvider().isPresent()).isTrue();
        assertThat(antora.getPlaybookProvider().getDownloadUrl()).isInstanceOf(URL.class);
        assertThat(antora.getPlaybookProvider().getDownloadUrl().toString())
            .isEqualTo("https://raw.githubusercontent.com/foo/bar/docs-build/antora-playbook-template.yml");
    }

    @Test
    void antoraWithPlaybookProviderOnGitLab() throws Exception {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(AntoraPlugin.class);
        AntoraExtension antora = project.getExtensions().getByType(AntoraExtension.class);
        antora.getPlaybookProvider().getHost().set("gitlab.com");
        antora.getPlaybookProvider().getRepository().set("foo/bar");
        assertThat(antora.getPlaybookProvider().isPresent()).isTrue();
        assertThat(antora.getPlaybookProvider().getDownloadUrl()).isInstanceOf(URL.class);
        assertThat(antora.getPlaybookProvider().getDownloadUrl().toString())
            .isEqualTo("https://gitlab.com/foo/bar/-/raw/docs-build/antora-playbook-template.yml");
    }

    @Test
    void antoraWhenCustomVersion() throws Exception {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(AntoraPlugin.class);
        AntoraExtension antora = project.getExtensions().getByType(AntoraExtension.class);
        antora.getVersion().set("3.0.3");
        NpxTask antoraTask = (NpxTask) project.getTasks().getByPath(":antora");
        assertThat(antoraTask.getArgs().get()).containsOnly("--package", "antora@3.0.3", "antora",
            "antora-playbook.yml");
    }

    @Test
    void antoraWhenCustomOptionsAsArray() throws Exception {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(AntoraPlugin.class);
        AntoraExtension antora = project.getExtensions().getByType(AntoraExtension.class);
        antora.getOptions().set(List.of("--fetch"));
        NpxTask antoraTask = (NpxTask) project.getTasks().getByPath(":antora");
        assertThat(antoraTask.getArgs().get()).containsOnly("--package", "antora", "antora", "--fetch",
            "antora-playbook.yml");
    }

    @Test
    void antoraWhenCustomOptionsAsMap() throws Exception {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(AntoraPlugin.class);
        AntoraExtension antora = project.getExtensions().getByType(AntoraExtension.class);
        Map<String, Object> options = new LinkedHashMap<>();
        options.put("clean", true);
        options.put("to-dir", "./public");
        options.put("attributes", List.of("idprefix", "idseparator=-"));
        antora.setOptions(options);
        NpxTask antoraTask = (NpxTask) project.getTasks().getByPath(":antora");
        assertThat(antoraTask.getArgs().get()).containsOnly("--package", "antora", "antora", "--clean", "--to-dir",
            "./public", "--attribute", "idprefix", "--attribute", "idseparator=-", "antora-playbook.yml");
    }

    @Test
    void antoraWhenUiBundleUrlOnAntoraTask() throws Exception {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(AntoraPlugin.class);
        AntoraTask antoraTask = project.getTasks().named("antora", AntoraTask.class).get();
        antoraTask.setUiBundleUrl("ui-bundle-from-task.zip");
        assertThat(antoraTask.getArgs().get()).containsOnly("--package", "antora", "antora", "--ui-bundle-url",
            "ui-bundle-from-task.zip", "antora-playbook.yml");
    }

    @Test
    void antoraWhenCustomAttributesAsMap() throws Exception {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(AntoraPlugin.class);
        AntoraExtension antora = project.getExtensions().getByType(AntoraExtension.class);
        Map<String, Object> options = new LinkedHashMap<>();
        Map<String, Object> attributes = new LinkedHashMap<>();
        attributes.put("idprefix", "");
        attributes.put("idseparator", "-");
        attributes.put("page-pagination", true);
        attributes.put("toc", false);
        options.put("attributes", attributes);
        antora.setOptions(options);
        NpxTask antoraTask = (NpxTask) project.getTasks().getByPath(":antora");
        assertThat(antoraTask.getArgs().get()).containsOnly("--package", "antora", "antora", "--attribute", "idprefix",
            "--attribute", "idseparator=-", "--attribute", "page-pagination", "--attribute", "!toc",
            "antora-playbook.yml");
    }

    @Test
    void antoraWhenCustomEnvironment() throws Exception {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(AntoraPlugin.class);
        AntoraExtension antora = project.getExtensions().getByType(AntoraExtension.class);
        Map<String, String> environment = new LinkedHashMap<>();
        environment.put("ANTORA_LOG_LEVEL", "error");
        antora.getEnvironment().set(Collections.unmodifiableMap(environment));
        NpxTask antoraTask = (NpxTask) project.getTasks().getByPath(":antora");
        Map<String, String> actual = antoraTask.getEnvironment().get();
        assertThat(actual).extracting("ANTORA_LOG_LEVEL").isEqualTo("error");
        assertThat(actual).extracting("IS_TTY").isEqualTo("true");
    }

    @Test
    void antoraWhenCustomPackages() throws Exception {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(AntoraPlugin.class);
        AntoraExtension antora = project.getExtensions().getByType(AntoraExtension.class);
        Map<String, String> packages = new LinkedHashMap<>();
        packages.put("my-extension", "latest");
        antora.getPackages().set(Collections.unmodifiableMap(packages));
        NpxTask antoraTask = (NpxTask) project.getTasks().getByPath(":antora");
        assertThat(antoraTask.getArgs().get()).containsOnly("--package", "antora", "--package", "my-extension@latest",
            "antora", "antora-playbook.yml");
    }

    @Test
    @SuppressWarnings("deprecation")
    void antoraWhenCustomDependencies() throws Exception {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(AntoraPlugin.class);
        AntoraExtension antora = project.getExtensions().getByType(AntoraExtension.class);
        Map<String, String> dependencies = new LinkedHashMap<>();
        dependencies.put("my-extension", "latest");
        antora.getDependencies().set(Collections.unmodifiableMap(dependencies));
        NpxTask antoraTask = (NpxTask) project.getTasks().getByPath(":antora");
        assertThat(antoraTask.getArgs().get()).containsOnly("--package", "antora", "--package", "my-extension@latest",
            "antora", "antora-playbook.yml");
    }

    @ParameterizedTest
    @MethodSource("gradleVersions")
    void antoraWhenDefaultsThenSuccess(GradleVersion gradleVersion) throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/defaults");
        BuildResult result = gradleRunner().withProjectDir(projectDir).build();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(projectDir.toPath().resolve("build/site/index.html")).existsNoFollowLinks().isRegularFile();
        assertThat(new File(projectDir, "package.json")).doesNotExist();
        assertThat(new File(projectDir, "node_modules")).doesNotExist();
        // NOTE: assumes Antora supports the IS_TTY environment variable
        assertThat(result.getOutput()).contains("Site generation complete!");
    }

    @Test
    void antoraWhenGlobalNodeModulesExistsThenSuccess() throws Exception {
        File globalNodeModules = new File(System.getProperty("user.home"), "node_modules");
        boolean globalNodeModulesExists = globalNodeModules.exists();
        try {
            if (globalNodeModulesExists == false) globalNodeModules.mkdir();
            File projectDir = prepareProjectDir(projectDirParent, "/defaults");
            BuildResult result = gradleRunner().withProjectDir(projectDir).build();
            assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
            assertThat(projectDir.toPath().resolve("build/site/index.html")).existsNoFollowLinks().isRegularFile();
            assertThat(result.getOutput()).contains("Detected the existence of $HOME/node_modules.");
        } finally {
            if (globalNodeModulesExists == false) globalNodeModules.delete();
        }
    }

    @Test
    void antoraWhenCustomVersionThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/custom-antora-version");
        BuildResult result = gradleRunner().withProjectDir(projectDir).build();
        assertThat(result.getOutput()).contains("@antora/cli: 3.0.3");
        File npxDir = new File(projectDir, ".gradle/npm/_npx");
        String[] npxProfiles = npxDir.list();
        assertThat(npxProfiles).hasSize(1);
        File npxProfileDir = new File(npxDir, npxProfiles[0]);
        File packageJson = npxProfileDir.toPath().resolve("package.json").toFile();
        assertThat(contentOf(packageJson).replaceAll("\\s+", ""))
            .isEqualTo("{\"dependencies\":{\"antora\":\"^3.0.3\"}}");
        File packageLockJson = npxProfileDir.toPath().resolve("package-lock.json").toFile();
        String packageLockJsonContent = contentOf(packageLockJson);
        assertThat(packageLockJsonContent).contains("\"lockfileVersion\": 3,");
        assertThat(packageLockJsonContent)
            .contains("\"resolved\": \"https://registry.npmjs.org/@antora/cli/-/cli-3.0.3.tgz\",");
    }

    @Test
    void antoraWhenExactNodeVersionThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/custom-node-version");
        BuildResult result = gradleRunnerWithArguments("antora", "-Pnode.version=" + NodeExtension.DEFAULT_NODE_VERSION)
            .withProjectDir(projectDir)
            .build();
        assertThat(result.task(":npmSetup").getOutcome()).isEqualTo(TaskOutcome.SKIPPED);
        assertThat(result.getOutput()).contains("Node.js version: v" + NodeExtension.DEFAULT_NODE_VERSION);
        assertThat(result.getOutput()).contains("npm version: v" + NodeExtension.DEFAULT_NPM_VERSION);
    }

    @Test
    void antoraWhenLtsNodeVersionThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/custom-node-version");
        BuildResult result = gradleRunnerWithArguments("antora", "-Pnode.version=lts", "-Pnode.npmVersion=8.19.3")
            .withProjectDir(projectDir)
            .build();
        assertThat(result.task(":npmSetup").getOutcome()).isEqualTo(TaskOutcome.SKIPPED);
        assertThat(result.getOutput()).contains("Node.js version: v18.13.0");
        assertThat(result.getOutput()).contains("npm version: v8.19.3");
    }

    @Test
    void antoraWhenLatestNodeVersionThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/custom-node-version");
        BuildResult result =
            gradleRunnerWithArguments("antora", "-Pnode.version=latest").withProjectDir(projectDir).build();
        assertThat(result.task(":npmSetup").getOutcome()).isEqualTo(TaskOutcome.SKIPPED);
        assertThat(result.getOutput()).contains("Node.js version: v19.3.0");
        assertThat(result.getOutput()).contains("npm version: v9.2.0");
    }

    @Test
    void antoraWhenFuzzyNodeVersionAndExactNpmVersionThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/custom-node-version");
        BuildResult result = gradleRunnerWithArguments("antora", "-Pnode.version=19", "-Pnode.npmVersion=8.19.3")
            .withProjectDir(projectDir)
            .build();
        assertThat(result.task(":npmSetup").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(result.getOutput()).contains("Node.js version: v19.3.0");
        assertThat(result.getOutput()).contains("npm version: v8.19.3");
        assertThat(result.getOutput()).doesNotContain("funding");
    }

    @Test
    void antoraWhenDownloadNodeDistDataThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/custom-node-version");
        File nodejsJson = new File(projectDir, ".gradle/nodejs.json");
        nodejsJson.delete();
        BuildResult result = gradleRunner().withProjectDir(projectDir).build();
        assertThat(nodejsJson).isFile();
        JsonArray releases = JsonParser.parseString(Files.readString(nodejsJson.toPath())).getAsJsonArray();
        Stream<JsonObject> releasesStream =
            StreamSupport.stream(releases.spliterator(), false).map(JsonElement::getAsJsonObject);
        Optional<JsonObject> release = releasesStream.filter(it -> it.getAsJsonPrimitive("lts").isString()).findFirst();
        assertThat(release).isPresent();
        assertThat(result.task(":npmSetup").getOutcome()).isEqualTo(TaskOutcome.SKIPPED);
        assertThat(result.getOutput()).contains("Node.js version: " + release.get().get("version").getAsString());
        assertThat(result.getOutput()).contains("npm version: v" + release.get().get("npm").getAsString());
    }

    @Test
    void antoraWhenDownloadNodeDistDataInOfflineModeThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/custom-node-version");
        File nodejsJson = new File(projectDir, ".gradle/nodejs.json");
        nodejsJson.delete();
        BuildResult result = gradleRunnerWithArguments("antora", "--offline").withProjectDir(projectDir).build();
        assertThat(nodejsJson).doesNotExist();
        assertThat(result.getOutput()).contains("Reverting to default version");
        assertThat(result.getOutput()).contains("Node.js version: v" + NodeExtension.DEFAULT_NODE_VERSION);
    }

    @Test
    void antoraWhenCustomPlaybookAsNullThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/custom-playbook-null");
        BuildResult result = gradleRunner().withProjectDir(projectDir).build();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(projectDir.toPath().resolve("build/site/index.html")).existsNoFollowLinks().isRegularFile();
    }

    @Test
    void antoraWhenCustomPlaybookAsFileThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/custom-playbook-file");
        BuildResult result = gradleRunner().withProjectDir(projectDir).build();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(projectDir.toPath().resolve("build/site/index.html")).existsNoFollowLinks().isRegularFile();
        assertThat(new File(projectDir, "package.json")).doesNotExist();
        assertThat(new File(projectDir, "node_modules")).doesNotExist();
    }

    @Test
    void antoraWhenCustomPlaybookAsStringThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/custom-playbook-path");
        BuildResult result = gradleRunner().withProjectDir(projectDir).build();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(projectDir.toPath().resolve("build/site/index.html")).existsNoFollowLinks().isRegularFile();
    }

    @Test
    void antoraWhenCustomAttributesThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/custom-attributes");
        BuildResult result = gradleRunner().withProjectDir(projectDir).build();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(result.getOutput()).contains("product: 'Bomb Dot Com'");
        assertThat(result.getOutput()).contains("'docker-tag': 'current'");
    }

    @Test
    void antoraWhenCustomPlaybookOptionThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/custom-playbook-option");
        BuildResult result =
            gradleRunnerWithArguments("antora", "--playbook", "site.yml").withProjectDir(projectDir).build();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(projectDir.toPath().resolve("build/site/index.html")).existsNoFollowLinks().isRegularFile();
    }

    @Test
    void antoraWhenCustomUiBundleUrlOptionThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/custom-ui-bundle-url-option");
        BuildResult result = gradleRunnerWithArguments("antora", "--ui-bundle-url",
            "https://gitlab.com/antora/antora-ui-default/-/jobs/artifacts/master/raw/build/ui-bundle.zip?job=bundle-stable")
                .withProjectDir(projectDir)
                .build();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(projectDir.toPath().resolve("build/site/index.html")).existsNoFollowLinks().isRegularFile();
    }

    @Test
    void antoraWhenCustomExtensionOptionThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/custom-extension-option");
        BuildResult result =
            gradleRunnerWithArguments("antora", "--extension", "@antora/atlas-extension").withProjectDir(projectDir)
                .build();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(projectDir.toPath().resolve("build/site/site-manifest.json")).existsNoFollowLinks().isRegularFile();
    }

    @Test
    void antoraWhenPlaybookProviderCheckLocalBranchThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/playbook-provider-local-branch", true);
        BuildResult result = gradleRunner().withProjectDir(projectDir).build();
        assertThat(projectDir.toPath().resolve("local-antora-playbook.yml")).existsNoFollowLinks().isRegularFile();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(projectDir.toPath().resolve("build/site/index.html")).existsNoFollowLinks().isRegularFile();
    }

    @Test
    void antoraWhenPlaybookProviderCheckLocalBranchFromLinkedWorktreeThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/playbook-provider-local-branch", true);
        File mainWorktreeDir = projectDir.toPath().resolveSibling("main").toFile();
        mainWorktreeDir.mkdir();
        Files.move(new File(projectDir, ".git").toPath(), new File(mainWorktreeDir, ".git").toPath());
        File worktreeGitdir = new File(mainWorktreeDir, ".git/worktrees/content-branch");
        worktreeGitdir.mkdirs();
        Files.write(new File(worktreeGitdir, "commondir").toPath(), List.of("../.."));
        Files.write(new File(projectDir, ".git").toPath(), List.of("gitdir: " + worktreeGitdir.getCanonicalPath()));
        BuildResult result = gradleRunner().withProjectDir(projectDir).build();
        assertThat(projectDir.toPath().resolve("local-antora-playbook.yml")).existsNoFollowLinks().isRegularFile();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(projectDir.toPath().resolve("build/site/index.html")).existsNoFollowLinks().isRegularFile();
    }

    @Test
    void antoraWhenPlaybookProviderWithPackagesThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/playbook-provider-with-packages", true);
        BuildResult result = gradleRunner().withProjectDir(projectDir).build();
        assertThat(projectDir.toPath().resolve(".antora-playbook.yml")).existsNoFollowLinks().isRegularFile();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(projectDir.toPath().resolve("build/site/index.html")).existsNoFollowLinks().isRegularFile();
    }

    @Test
    void antoraWhenPlaybookProviderWithAntoraPackageThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/playbook-provider-with-antora-package", true);
        BuildResult result = gradleRunner().withProjectDir(projectDir).build();
        assertThat(projectDir.toPath().resolve("antora-playbook.yml")).existsNoFollowLinks().isRegularFile();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        // NOTE: assumes Antora supports the IS_TTY environment variable
        assertThat(result.getOutput()).contains("@antora/cli: 3.2.0-alpha.2");
    }

    // NOTE this test will use the name of the remote tracking branch, or main if none is defined
    @Test
    void antoraWhenPlaybookProviderRemoteBranchThenSuccess() throws Exception {
        Repository repo = new RepositoryBuilder().setGitDir(new File(".git")).build();
        String currentBranch = repo.getBranch();
        String remoteBranch = new BranchConfig(repo.getConfig(), currentBranch).getRemoteTrackingBranch();
        currentBranch = remoteBranch == null ? "main" : repo.shortenRemoteBranchName(remoteBranch);
        File projectDir = prepareProjectDir(projectDirParent, "/playbook-provider-remote-branch");
        BuildResult result =
            gradleRunnerWithArguments("-PcurrentBranch=" + currentBranch, "antora").withProjectDir(projectDir).build();
        assertThat(projectDir.toPath().resolve("antora-playbook.yml")).existsNoFollowLinks().isRegularFile();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(projectDir.toPath().resolve("build/site/index.html")).existsNoFollowLinks().isRegularFile();
    }

    @Test
    void antoraWhenPlaybookProviderRemoteBranchMissingThenFailure() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/playbook-provider-remote-branch");
        BuildResult result =
            gradleRunnerWithArguments("-PcurrentBranch=does-not-exist", "antora").withProjectDir(projectDir)
                .buildAndFail();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.FAILED);
        assertThat(projectDir.toPath().resolve("antora-playbook.yml")).doesNotExist();
        assertThat(result.getOutput()).contains("Failed to download playbook from specified provider");
    }

    @Test
    void antoraWhenCustomEnvironmentThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/custom-env");
        BuildResult result = gradleRunner().withProjectDir(projectDir).build();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        File sitemapFile = projectDir.toPath().resolve("build/site/sitemap.xml").toFile();
        assertThat(sitemapFile).exists().isFile();
        assertThat(contentOf(sitemapFile)).contains("https://docs.example.org/");
        if (!new PlatformHelper().isWindows()) {
            assertThat(result.getOutput()).contains("HOME: " + System.getenv("HOME"));
        }
    }

    @Test
    void antoraWithPackagesThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/with-packages");
        gradleRunner().withProjectDir(projectDir).build();
        File nodeModules = new File(projectDir, "node_modules");
        assertThat(nodeModules).doesNotExist();
        File packageLockJson = new File(projectDir, "package-lock.json");
        assertThat(packageLockJson).doesNotExist();
        File npxDir = new File(projectDir, ".gradle/npm/_npx");
        String[] npxProfiles = npxDir.list();
        assertThat(npxProfiles).hasSize(1);
        File npxProfileDir = new File(npxDir, npxProfiles[0]);
        File packageJson = npxProfileDir.toPath().resolve("package.json").toFile();
        assertThat(contentOf(packageJson)).contains("\"@antora/collector-extension\"");
    }

    @Test
    void antoraWithDependenciesThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/with-dependencies");
        gradleRunner().withProjectDir(projectDir).build();
        File nodeModules = new File(projectDir, "node_modules");
        assertThat(nodeModules).doesNotExist();
        File packageLockJson = new File(projectDir, "package-lock.json");
        assertThat(packageLockJson).doesNotExist();
        File npxDir = new File(projectDir, ".gradle/npm/_npx");
        String[] npxProfiles = npxDir.list();
        assertThat(npxProfiles).hasSize(1);
        File npxProfileDir = new File(npxDir, npxProfiles[0]);
        File packageJson = npxProfileDir.toPath().resolve("package.json").toFile();
        assertThat(contentOf(packageJson)).contains("\"@antora/collector-extension\"");
    }

    @Test
    void antoraWhenIsTTYIsFalseThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/no-tty");
        Map<String, String> env = new LinkedHashMap<>(System.getenv());
        env.put("IS_TTY", "false");
        String output = gradleRunner().withEnvironment(Collections.unmodifiableMap(env))
            .withProjectDir(projectDir)
            .build()
            .getOutput();
        assertThat(output).contains("FORCE_COLOR=<not set>");
        assertThat(output).contains("IS_TTY=false");
    }

    @Test
    void antoraWhenLocalInstallThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/local-install");
        GradleRunner gradle = gradleRunner().withProjectDir(projectDir);
        BuildResult result = gradle.build();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(result.task(":npmCiOrInstall").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        File nodeModules = new File(projectDir, "node_modules");
        assertThat(nodeModules).isDirectory();
        File packageLockJson = new File(projectDir, "package-lock.json");
        assertThat(packageLockJson).doesNotExist();
        File antoraSiteGeneratorPackageJson = new File(projectDir, "node_modules/@antora/site-generator/package.json");
        assertThat(antoraSiteGeneratorPackageJson).exists().isFile();
        File npxDir = new File(projectDir, ".gradle/npm/_npx");
        String[] npxProfiles = npxDir.list();
        assertThat(npxProfiles).hasSize(1);
        File npxProfileDir = new File(npxDir, npxProfiles[0]);
        File packageJson = npxProfileDir.toPath().resolve("package.json").toFile();
        assertThat(contentOf(packageJson).replaceAll("\\s+", ""))
            .isEqualTo("{\"dependencies\":{\"@antora/cli\":\"^3.1.0\"}}");
        result = gradle.build();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(result.task(":npmCiOrInstall").getOutcome()).isEqualTo(TaskOutcome.UP_TO_DATE);
    }

    @Test
    void antoraWhenLocalCiThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/local-ci");
        GradleRunner gradle = gradleRunner().withProjectDir(projectDir);
        BuildResult result = gradle.build();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(result.task(":npmCiOrInstall").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        File nodeModules = new File(projectDir, "node_modules");
        assertThat(nodeModules).exists().isDirectory();
        File packageLockJson = new File(projectDir, "package-lock.json");
        assertThat(packageLockJson).exists().isFile();
        File yargsParserPackageJson = new File(projectDir, "node_modules/yargs-parser/package.json");
        assertThat(yargsParserPackageJson).exists().isFile();
        String packageLockJsonContent = contentOf(packageLockJson);
        assertThat(packageLockJsonContent).contains("\"lockfileVersion\": 2,");
        assertThat(packageLockJsonContent).contains("\"node_modules/yargs-parser\"");
        result = gradle.build();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(result.task(":npmCiOrInstall").getOutcome()).isEqualTo(TaskOutcome.UP_TO_DATE);
    }

    @Test
    void antoraWhenLocalInstallWithNpmConfigThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/with-npmrc");
        GradleRunner gradle = gradleRunner().withProjectDir(projectDir);
        BuildResult result =
            gradle.withArguments("npmSetup", "-Pnode.npmVersion=" + NodeExtension.DEFAULT_NPM_VERSION).build();
        assertThat(result.task(":npmSetup").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(result.getOutput()).doesNotContain("funding");
        assertThat(result.getOutput()).doesNotContain("vulnerabilities");
        result = gradle.withArguments("antora", "-Pnode.npmVersion=" + NodeExtension.DEFAULT_NPM_VERSION).build();
        assertThat(result.task(":npmSetup").getOutcome()).isEqualTo(TaskOutcome.UP_TO_DATE);
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(result.task(":npmCiOrInstall").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(new File(projectDir, "node_modules")).exists().isDirectory();
        assertThat(result.getOutput()).doesNotContain("funding");
        assertThat(result.getOutput()).doesNotContain("vulnerabilities");
    }

    @Test
    void antoraWithoutLocalInstallThenSuccess() throws Exception {
        File projectDir = prepareProjectDir(projectDirParent, "/local-no-dependencies");
        GradleRunner gradle = gradleRunner().withProjectDir(projectDir);
        BuildResult result = gradle.withArguments("antora").build();
        assertThat(result.task(":antora").getOutcome()).isEqualTo(TaskOutcome.SUCCESS);
        assertThat(result.getTasks().stream().filter(t -> t.getPath().equals(":npmCiOrInstall")).findFirst())
            .isNotPresent();
        assertThat(new File(projectDir, "node_modules")).doesNotExist();
    }

    private static GradleRunner gradleRunner() {
        return gradleRunnerWithArguments("antora");
    }

    private static GradleRunner gradleRunnerWithArguments(String... arguments) {
        return GradleRunner.create().forwardOutput().withArguments(arguments).withPluginClasspath();
    }

    private static File prepareProjectDir(File parentDir, String resourceName) throws Exception {
        return prepareProjectDir(parentDir, resourceName, false);
    }

    private static File prepareProjectDir(File parentDir, String resourceName, boolean initGit) throws Exception {
        Path projectDirPath = parentDir.toPath().resolve("project");
        File projectDir = projectDirPath.toFile();
        copyRecursive(Paths.get(AntoraPluginTest.class.getResource(resourceName).toURI()), projectDirPath);
        Files.copy(Paths.get(AntoraPluginTest.class.getResource("/ui-bundle.zip").toURI()),
            new File(projectDir, "ui-bundle.zip").toPath());
        URL testkitGradleProperties = AntoraPluginTest.class.getResource("/testkit-gradle.properties");
        if (testkitGradleProperties != null) {
            Files.copy(Paths.get(testkitGradleProperties.toURI()), new File(projectDir, "gradle.properties").toPath());
        }
        if (initGit) {
            Git repo = Git.init().setDirectory(projectDir).setInitialBranch("main").call();
            repo.add().addFilepattern(".").call();
            repo.commit().setAuthor("Test", "test@example.org").setMessage("init").call();
        }
        return projectDir;
    }

    private static void copyRecursive(Path source, Path target, CopyOption... options) throws IOException {
        Files.walkFileTree(source, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                Files.createDirectories(target.resolve(source.relativize(dir)));
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.copy(file, target.resolve(source.relativize(file)), options);
                return FileVisitResult.CONTINUE;
            }
        });
    }
}
