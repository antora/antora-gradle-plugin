#!/bin/bash

if [ -z "$RELEASE_DEPLOY_KEY" ]; then
  declare -n RELEASE_DEPLOY_KEY="RELEASE_DEPLOY_KEY_$GITLAB_USER_LOGIN"
  if [ -z "$RELEASE_DEPLOY_KEY" ]; then
    echo No release deploy key \(RELEASE_DEPLOY_KEY or RELEASE_DEPLOY_KEY_$GITLAB_USER_LOGIN\) defined. Halting release.
    exit 1
  fi
fi
if [ -z $GRADLE_PUBLISH_KEY ]; then
  declare -n GRADLE_PUBLISH_KEY="GRADLE_PUBLISH_KEY_$GITLAB_USER_LOGIN"
  if [ -z $GRADLE_PUBLISH_KEY ]; then
    echo No Gradle publish key \(GRADLE_PUBLISH_KEY or GRADLE_PUBLISH_KEY_$GITLAB_USER_LOGIN\) defined. Halting release.
    exit 1
  fi
fi
if [ -z $GRADLE_PUBLISH_SECRET ]; then
  declare -n GRADLE_PUBLISH_SECRET="GRADLE_PUBLISH_SECRET_$GITLAB_USER_LOGIN"
  if [ -z $GRADLE_PUBLISH_SECRET ]; then
    echo No Gradle publish secret \(GRADLE_PUBLISH_SECRET or GRADLE_PUBLISH_SECRET_$GITLAB_USER_LOGIN\) defined. Halting release.
    exit 1
  fi
fi
# RELEASE_VERSION must be an exact version number
if [ -z $RELEASE_VERSION ]; then
  echo No release version \(RELEASE_VERSION\) defined. Halting release.
  exit 1
fi

export RELEASE_DATE="$(date +%Y-%m-%d)"

RELEASE_BRANCH=$CI_COMMIT_BRANCH

# set up SSH auth using ssh-agent
mkdir -p -m 700 $HOME/.ssh
ssh-keygen -F gitlab.com >/dev/null 2>&1 || ssh-keyscan -H -t rsa gitlab.com >> $HOME/.ssh/known_hosts 2>/dev/null
eval $(ssh-agent -s) >/dev/null
if [ -f "$RELEASE_DEPLOY_KEY" ]; then
  chmod 600 $RELEASE_DEPLOY_KEY
  ssh-add -q $RELEASE_DEPLOY_KEY
else
  echo -n "$RELEASE_DEPLOY_KEY" | ssh-add -q -
fi
exit_code=$?
if [ $exit_code -gt 0 ]; then
  exit $exit_code
fi
echo Deploy key identity added to SSH agent.

# configure git to push changes
git config --local user.name "$GITLAB_USER_NAME"
git config --local user.email "$GITLAB_USER_EMAIL"
git remote set-url origin git@gitlab.com:$CI_PROJECT_PATH.git
git fetch --depth ${GIT_DEPTH:-5} --update-shallow origin $RELEASE_BRANCH

# make sure the release branch exists as a local branch
git checkout -b $RELEASE_BRANCH -t origin/$RELEASE_BRANCH

if [ "$(git rev-parse $RELEASE_BRANCH)" != $CI_COMMIT_SHA ]; then
  echo $RELEASE_BRANCH moved forward from $CI_COMMIT_SHA. Halting release.
  exit 1
fi

# release!
(
  set -e
  ./scripts/version.sh
  git commit -a -m "release $RELEASE_VERSION"
  git tag -m "version $RELEASE_VERSION" v$RELEASE_VERSION
  git push origin $(git describe --tags --exact-match)
  ./gradlew publishPlugins -Pgradle.publish.key=$GRADLE_PUBLISH_KEY -Pgradle.publish.secret=$GRADLE_PUBLISH_SECRET
  ./scripts/postrelease.sh
  git commit -a -m 'prepare branch for development [skip ci]'
  git push origin $RELEASE_BRANCH
)

exit_code=$?
git status -s -b
eval $(ssh-agent -k) >/dev/null
exit $exit_code
