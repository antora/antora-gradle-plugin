= Use the Plugin

== Prerequisites

The Antora plugin requires at least Java {version-java-min} and Gradle {version-gradle-min}.
You don't need Node.js preinstalled to use this plugin, the plugin sets up the Node.js runtime for you automatically.
If you want to override the default Node.js version used by the plugin, see xref:configure.adoc#node-version[Configure the Node.js version].

[#apply]
== Apply the plugin

To use the plugin, you must first declare it in the `plugins` block at the top of your Gradle build script ([.path]_build.gradle_).

.build.gradle
[,groovy,subs=attributes+]
----
plugins {
    id 'org.antora' version '{release-version}'
}
----

The plugin declaration automatically adds the `antora` task to your Gradle build as well as the xref:configure.adoc[antora extension block] to configure it.

[#run-antora]
== Run the antora task

You can use the `antora` task to run Antora on a playbook file without having to configure the plugin.
To do so, run the task named `antora` using Gradle (e.g., `./gradlew`):

 $ ./gradlew antora

This task is the equivalent of running the `antora` command once the *antora* npm package is installed (except, in this case, the playbook path is not required).
You can configure Gradle to only show the output from the Antora command by adding the `-q` option:

 $ ./gradlew -q antora

NOTE: If you don't yet have `gradlew` in your project, see https://gradle.org/install/#with-the-gradle-wrapper[the Gradle wrapper] to learn how to set it up.
Once it's present in the project, you don't need Gradle installed in order to run Gradle.

A Gradle plugin cannot tell if Gradle is running in an interactive terminal.
Therefore, the Antora plugin automatically sets the environment variable `IS_TTY=true` so Antora uses the colorized pretty log format by default.
If you want to override this default behavior, such as in a CI environment, you can set `IS_TTY=false` when running Gradle:

 $ IS_TTY=false ./gradlew antora

If your playbook file is not named [.path]_antora-playbook.yml_, you will need to specify its location using the `--playbook` task option:

 $ ./gradlew antora --playbook my-antora-playbook.yml

Under the covers, the plugin builds a commandline string that invokes the `antora` command through `npx`.
To see the command that the plugin runs, set the logging level to info using Gradle's `-i` CLI option, then look for the command under the heading *> Task :antora* (or similar).

 $ ./gradlew -i antora

The CLI options for the `antora` task are a subset of the CLI options supported by Antora's `antora` command.
To list the subset of Antora CLI options, run:

 $ ./gradlew help --task antora

An option listed has the same functionality as the xref:antora:cli:options.adoc[matching CLI option in Antora].

You can configure more of Antora's options using the xref:configure.adoc#antora[antora extension block in your Gradle build script].
You can also configure the Node.js runtime on which Antora runs using the xref:configure.adoc#node[node extension block in your Gradle build script].
